from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from accounts.forms import LoginForm, SignupForm


# , logout in auth
# from django.contrib.auth.models import User
# Create your VIEWS here.


#  File "/Users/tealeaf/hack-reactor/projects/week5/project-alpha-apr/tests/test_feature_08.py", line 67, in test_projects_list_shows_no_projects_when_member_of_one
#     self.assertEqual(
# AssertionError: 302 != 200 : Redirected for logged in user


def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect("list_projects")
    else:
        form = LoginForm()
    context = {"form": form}
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


def user_signup(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                # create new user with those values
                # save to variable
                user = User.objects.create_user(
                    username,
                    password=password,
                )
                # login w user just created
                login(request, user)

                return redirect("list_projects")
            else:
                form.add_error("password", "Passwords do not match")
    else:
        form = SignupForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)
