from django.contrib import admin
from projects.models import Project

# Register your models here.
# Registering a model in the admin
# When registering a model, make sure you do two things:

# Create a class that inherits from admin.ModelAdmin
# Register the class along with your model, using admin.site.register()

admin.site.register(Project)


class ExpenseCategoryAdmin:
    listdisplay = ("name", "description", "owner")
