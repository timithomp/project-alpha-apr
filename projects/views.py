from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from projects.forms import ProjectForm


# from projects.models import Projects
# Create your views here.
# 1 Create a view that will get all of the instances of the Project model and put them in the context for the template.
# 2 Register that view in the projects app
#  for the path "" and the name "list_projects" in a new file named projects/urls.py.
# 3 Include the url patterns from the projects app in the tracker project with the prefix "projects/".
from django.contrib.auth.decorators import login_required


@login_required
def list_projects(request):
    list_projects = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": list_projects,
    }
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, project_id):
    project = get_object_or_404(Project, id=project_id)
    context = {
        "show_project": project,
    }
    return render(request, "projects/show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
        context = {"form": form}
        return render(request, "projects/create_project.html", context)
