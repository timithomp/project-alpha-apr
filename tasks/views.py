# Create a view that will show a form to create an instance of the Task model for all properties except the is_completed field.
#     (You may want to use a ModelForm.)
# In this view, you do not assign the owner of the task.
#     This means that you will use form.save() ModuleNotFoundError form.save(False).
# The view must only be accessible by people who are logged in.
# When the view successfully handles the form submission,
#      have it redirect to the list of projects
# Register that view in the tasks app for the path "create/"
#     and the name "create_task" in a new file named tasks/urls.py.
from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("home")
    else:
        form = TaskForm()
        context = {"form": form}
        return render(request, "projects/create_project.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "show_my_tasks": tasks,
    }
    return render(request, "tasks/show_my_tasks.html", context)
